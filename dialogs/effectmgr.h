/***

    KunGang - Simple Video Editor
    Copyright (C) 2019  Sumoon.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

***/

#ifndef EFFECTMGRDIALOG_H
#define EFFECTMGRDIALOG_H

#include <QDialog>
#include <QVector>
#include <QComboBox>
#include <QNetworkReply>
#include <QProgressBar>
#include <QLabel>

#include "project/media.h"

/**
 * @brief The EffectMgrDialog class
 *
 * Dialog to set up proxy generation of footage. This dialog can be called from anywhere provided it's given a valid
 * array of Media and will start all proxy generation.
 */
class EffectMgrDialog : public QDialog {
  Q_OBJECT
public:
  /**
   * @brief EffectMgrDialog Constructor
   * @param parent
   *
   * Parent widget to become modal to.
   *
   * @param footage
   *
   * List of Footage items to process.
   */
  EffectMgrDialog(QWidget* parent);
public slots:
  void load_effect();

  /**
   * @brief Accept changes
   *
   * Called when the user clicks OK on the dialog. Verifies all proxies, asking the user whether they want to overwrite
   * existing proxies if necessary, and if everything is valid, queues the footage with ProxyGenerator.
   */
  virtual void accept() override;
private:
  int line;
  QPushButton*  load_btn;
  QProgressBar*  progressBar;
  QLineEdit* effectname_edit;
  QLabel*  info;


private slots:
  /**
   * @brief Slot when the user changes the location
   *
   * Triggered when the user changes the index in the location combobox.
   *
   * @param i
   *
   * location_combobox's new selected index
   */
  void updateDataReadProgress(qint64 s,qint64 e);
  void finished_slot(QNetworkReply *reply);
};

#endif // EFFECTMGRDIALOG_H
