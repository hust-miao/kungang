/***

    Kungang - Non-Linear Video Editor
    Copyright (C) 2021 Kungang Team

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

***/

#ifndef VIPDIALOG_H
#define VIPDIALOG_H

#include <QDialog>

/**
 * @brief The AboutDialog class
 *
 * The About dialog (accessible through Help > About). Contains license and version information. This can be run from
 * anywhere
 */
class VipDialog : public QDialog
{
  Q_OBJECT
public slots:
    void openUrl(const QString& url);  
public:
  /**
   * @brief VipDialog Constructor
   *
   * Creates VIP dialog.
   *
   * @param parent
   *
   * QWidget parent object. Usually this will be MainWindow.
   */
  explicit VipDialog(QWidget *parent = nullptr);
};

#endif // VIPDIALOG_H
