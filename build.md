# Compiling on Windows
you'll need to download MSYS2 (the build environment for MingW). Download it at: https://www.msys2.org/.
In MSYS2, you can now use its package manager pacman install dependencies.
## set mirror for quick downloading 
### 64-bit
vi /etc/pacman.d/mirrorlist.mingw64
Server = http://mirrors.ustc.edu.cn/msys2/mingw/x86_64
### 32-bit
vi /etc/pacman.d/mirrorlist.mingw32
Server = http://mirrors.ustc.edu.cn/msys2/mingw/i686

## update
Make sure your MSYS2 and its packages are up to date with the following command:
```
pacman -Syu
```
## for different ARCH:
64-bit:
pacman -S mingw-w64-x86_64-toolchain make

32-bit:
pacman -S mingw-w64-i686-toolchain make

Now to install Qt 5
64-bit:
pacman -S mingw-w64-x86_64-qt5

32-bit:
pacman -S mingw-w64-i686-qt5

Next we'll install FFmpeg the same way.
64-bit:
pacman -S mingw-w64-x86_64-ffmpeg

32-bit:
pacman -S mingw-w64-i686-ffmpeg


## To install through git, first install it:
pacman -S git

## Then clone the repository:
git clone this_repo


## Generate a Makefile:
qmake kungang.pro
>> if you cannot find qmake
```
export PATH=$PATH:/mingw64/bin
``` 

## If you didn't install Frei0r earlier, you'll need to disable it. Otherwise skip this step.
qmake "DEFINES+=NOFREI0R" kungang.pro


## should work now
make -f Makefile.Release
