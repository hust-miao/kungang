<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="id_ID">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../dialogs/aboutdialog.cpp" line="33"/>
        <source>About KunGang</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/aboutdialog.cpp" line="46"/>
        <source>KunGang is a simple video editor. This software is free and protected by the GNU GPL. Its was originally developed in Feb 2020, based on Olive(https://www.olivevideoeditor.org)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/aboutdialog.cpp" line="48"/>
        <source>KunGang Team is obliged to inform users that KunGang source code is available for download from its website.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ActionSearch</name>
    <message>
        <location filename="../dialogs/actionsearch.cpp" line="57"/>
        <source>Search for action...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AdvancedVideoDialog</name>
    <message>
        <location filename="../dialogs/advancedvideodialog.cpp" line="41"/>
        <source>Advanced Video Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/advancedvideodialog.cpp" line="53"/>
        <source>Pixel Format:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/advancedvideodialog.cpp" line="77"/>
        <source>Threads:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Audio</name>
    <message>
        <location filename="../rendering/audio.cpp" line="347"/>
        <source>Recording %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AudioNoiseEffect</name>
    <message>
        <location filename="../effects/internal/audionoiseeffect.cpp" line="24"/>
        <source>Amount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/audionoiseeffect.cpp" line="30"/>
        <source>Mix</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AutoCutSilenceDialog</name>
    <message>
        <location filename="../dialogs/autocutsilencedialog.cpp" line="38"/>
        <source>Cut Silence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/autocutsilencedialog.cpp" line="44"/>
        <source>Attack Threshold:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/autocutsilencedialog.cpp" line="49"/>
        <source>Attack Time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/autocutsilencedialog.cpp" line="54"/>
        <source>Release Threshold:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/autocutsilencedialog.cpp" line="59"/>
        <source>Release Time:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Cacher</name>
    <message>
        <location filename="../rendering/cacher.cpp" line="922"/>
        <location filename="../rendering/cacher.cpp" line="931"/>
        <source>Could not open %1 - %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChannelLayoutName</name>
    <message>
        <location filename="../project/media.cpp" line="53"/>
        <source>Invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="54"/>
        <source>Mono</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="55"/>
        <source>Stereo</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ClipPropertiesDialog</name>
    <message>
        <location filename="../dialogs/clippropertiesdialog.cpp" line="14"/>
        <source>&quot;%1&quot; Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/clippropertiesdialog.cpp" line="15"/>
        <source>Multiple Clip Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/clippropertiesdialog.cpp" line="24"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/clippropertiesdialog.cpp" line="33"/>
        <source>Duration:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/clippropertiesdialog.cpp" line="71"/>
        <source>(multiple)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CollapsibleWidget</name>
    <message>
        <location filename="../ui/collapsiblewidget.cpp" line="56"/>
        <source>Save current effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/collapsiblewidget.cpp" line="59"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/collapsiblewidget.cpp" line="62"/>
        <source>&lt;untitled&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorButton</name>
    <message>
        <location filename="../ui/colorbutton.cpp" line="47"/>
        <source>Set Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CornerPinEffect</name>
    <message>
        <location filename="../effects/internal/cornerpineffect.cpp" line="30"/>
        <source>Top Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/cornerpineffect.cpp" line="34"/>
        <source>Top Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/cornerpineffect.cpp" line="38"/>
        <source>Bottom Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/cornerpineffect.cpp" line="42"/>
        <source>Bottom Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/cornerpineffect.cpp" line="46"/>
        <source>Perspective</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DebugDialog</name>
    <message>
        <location filename="../dialogs/debugdialog.cpp" line="44"/>
        <source>Debug Log</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DemoNotice</name>
    <message>
        <location filename="../dialogs/demonotice.cpp" line="30"/>
        <location filename="../dialogs/demonotice.cpp" line="45"/>
        <source>Welcome to Olive!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/demonotice.cpp" line="47"/>
        <source>Olive is a free open-source video editor released under the GNU GPL. If you have paid for this software, you have been scammed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/demonotice.cpp" line="49"/>
        <source>This software is currently in ALPHA which means it is unstable and very likely to crash, have bugs, and have missing features. We offer no warranty so use at your own risk. Please report any bugs or feature requests at %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/demonotice.cpp" line="51"/>
        <source>Thank you for trying Olive and we hope you enjoy it!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Effect</name>
    <message>
        <location filename="../effects/effect.cpp" line="102"/>
        <source>Invalid effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="103"/>
        <source>No candidate for effect &apos;%1&apos;. This effect may be corrupt. Try reinstalling it or Olive.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="575"/>
        <source>Save effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="576"/>
        <source>Enter effect name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="596"/>
        <source>Save Effect Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="598"/>
        <location filename="../effects/effect.cpp" line="608"/>
        <source>Effect XML Settings %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="471"/>
        <source>Save Settings Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="472"/>
        <source>Failed to open &quot;%1&quot; for writing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="606"/>
        <source>Load Effect Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="562"/>
        <location filename="../effects/effect.cpp" line="622"/>
        <location filename="../effects/effect.cpp" line="814"/>
        <source>Load Settings Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="563"/>
        <location filename="../effects/effect.cpp" line="623"/>
        <source>Failed to open &quot;%1&quot; for reading.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effect.cpp" line="815"/>
        <source>This settings file doesn&apos;t match this effect.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EffectControls</name>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="335"/>
        <source>(none)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="522"/>
        <source>Effects: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="524"/>
        <source>Add Video Effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="525"/>
        <source>VIDEO EFFECTS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="526"/>
        <source>Add Video Transition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="527"/>
        <source>Add Audio Effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="528"/>
        <source>AUDIO EFFECTS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/effectcontrols.cpp" line="529"/>
        <source>Add Audio Transition</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EffectMgrDialog</name>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="50"/>
        <location filename="../dialogs/effectmgr.cpp" line="170"/>
        <source>Effect Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="59"/>
        <source>input code to download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="61"/>
        <location filename="../dialogs/effectmgr.cpp" line="75"/>
        <source>Get Effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="77"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="93"/>
        <source>downloading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="109"/>
        <location filename="../dialogs/effectmgr.cpp" line="118"/>
        <source>Network error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="130"/>
        <source>No network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="138"/>
        <location filename="../dialogs/effectmgr.cpp" line="149"/>
        <source>Write file error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="154"/>
        <source>extracting...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="161"/>
        <source>well done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="170"/>
        <source>Effect has been downloaded successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/effectmgr.cpp" line="174"/>
        <source>invalid zip file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EffectRow</name>
    <message>
        <location filename="../effects/effectrow.cpp" line="107"/>
        <source>Disable Keyframes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effectrow.cpp" line="108"/>
        <source>Disabling keyframes will delete all current keyframes. Are you sure you want to do this?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EffectUI</name>
    <message>
        <location filename="../ui/effectui.cpp" line="55"/>
        <source>%1 (Opening)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="57"/>
        <source>%1 (Closing)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="160"/>
        <source>%1 (multiple)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="386"/>
        <source>Cu&amp;t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="389"/>
        <source>&amp;Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="400"/>
        <source>Move &amp;Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="404"/>
        <source>Move &amp;Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="409"/>
        <source>D&amp;elete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="426"/>
        <source>Load Settings From File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/effectui.cpp" line="428"/>
        <source>Save Settings to File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EmbeddedFileChooser</name>
    <message>
        <location filename="../ui/embeddedfilechooser.cpp" line="52"/>
        <source>File:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExportDialog</name>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="75"/>
        <source>Export &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="128"/>
        <source>Unknown codec name %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="342"/>
        <source>Export Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="343"/>
        <source>Export failed - %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="373"/>
        <source>Export Successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="374"/>
        <source>Would you like to open containing folder?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="393"/>
        <source>Invalid dimensions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="394"/>
        <source>Export width and height must both be even numbers/divisible by 2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="450"/>
        <source>Invalid codec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="451"/>
        <source>Couldn&apos;t determine output parameters for the selected codec. This is a bug, please contact the developers.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="520"/>
        <source>Invalid format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="521"/>
        <source>Couldn&apos;t determine output format. This is a bug, please contact the developers.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="528"/>
        <source>Export Media</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="615"/>
        <source>%p% (Total: %1:%2:%3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="620"/>
        <source>%p% (ETA: %1:%2:%3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="635"/>
        <source>Quality-based (Constant Rate Factor)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="639"/>
        <source>Constant Bitrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="648"/>
        <location filename="../dialogs/exportdialog.cpp" line="654"/>
        <source>Invalid Codec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="649"/>
        <source>Failed to find a suitable encoder for this codec. Export will likely fail.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="655"/>
        <source>Failed to find pixel format for this encoder. Export will likely fail.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="668"/>
        <source>Bitrate (Mbps):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="672"/>
        <source>Quality (CRF):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="675"/>
        <source>Quality Factor:

0 = lossless
17-18 = visually lossless (compressed, but unnoticeable)
23 = high quality
51 = lowest quality possible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="678"/>
        <source>Target File Size (MB):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="714"/>
        <source>Format:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="723"/>
        <source>Range:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="726"/>
        <source>Entire Sequence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="727"/>
        <source>In to Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="734"/>
        <source>Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="740"/>
        <location filename="../dialogs/exportdialog.cpp" line="783"/>
        <source>Codec:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="744"/>
        <source>Width:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="749"/>
        <source>Height:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="754"/>
        <source>Frame Rate:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="760"/>
        <source>Compression Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="771"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="778"/>
        <source>Audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="787"/>
        <source>Sampling Rate:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="793"/>
        <source>Bitrate (Kbps/CBR):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="819"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/exportdialog.cpp" line="825"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExportThread</name>
    <message>
        <location filename="../rendering/exportthread.cpp" line="79"/>
        <source>failed to send frame to encoder (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="90"/>
        <source>failed to receive packet from encoder (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="113"/>
        <source>could not video encoder for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="122"/>
        <source>could not allocate video stream</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="131"/>
        <source>could not allocate video encoding context</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="180"/>
        <source>could not open output video encoder (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="188"/>
        <source>could not copy video encoder parameters to output stream (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="227"/>
        <source>could not audio encoder for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="235"/>
        <source>could not allocate audio stream</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="249"/>
        <source>could not allocate audio encoding context</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="274"/>
        <source>could not open output audio encoder (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="282"/>
        <source>could not copy audio encoder parameters to output stream (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="319"/>
        <source>could not allocate audio buffer (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="350"/>
        <source>could not create output format context</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="360"/>
        <source>could not open output file (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="396"/>
        <source>could not write output file header (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rendering/exportthread.cpp" line="600"/>
        <source>could not write output file trailer (%1)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FillLeftRightEffect</name>
    <message>
        <location filename="../effects/internal/fillleftrighteffect.cpp" line="27"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/fillleftrighteffect.cpp" line="29"/>
        <source>Fill Left with Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/fillleftrighteffect.cpp" line="30"/>
        <source>Fill Right with Left</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Frei0rEffect</name>
    <message>
        <location filename="../effects/internal/frei0reffect.cpp" line="54"/>
        <source>Error loading Frei0r plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/frei0reffect.cpp" line="55"/>
        <source>Failed to load Frei0r plugin &quot;%1&quot;: %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GraphEditor</name>
    <message>
        <location filename="../panels/grapheditor.cpp" line="140"/>
        <source>Graph Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/grapheditor.cpp" line="141"/>
        <source>Linear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/grapheditor.cpp" line="142"/>
        <source>Bezier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/grapheditor.cpp" line="143"/>
        <source>Hold</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GraphView</name>
    <message>
        <location filename="../ui/graphview.cpp" line="80"/>
        <source>Zoom to Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/graphview.cpp" line="87"/>
        <source>Zoom to Show All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/graphview.cpp" line="96"/>
        <source>Reset View</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InterlacingName</name>
    <message>
        <location filename="../project/media.cpp" line="44"/>
        <source>None (Progressive)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="45"/>
        <source>Top Field First</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="46"/>
        <source>Bottom Field First</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="47"/>
        <source>Invalid</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeyframeNavigator</name>
    <message>
        <location filename="../ui/keyframenavigator.cpp" line="77"/>
        <source>Enable Keyframes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeyframeView</name>
    <message>
        <location filename="../ui/keyframeview.cpp" line="74"/>
        <source>Linear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/keyframeview.cpp" line="76"/>
        <source>Bezier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/keyframeview.cpp" line="78"/>
        <source>Hold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/keyframeview.cpp" line="81"/>
        <source>Graph Editor</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LabelSlider</name>
    <message>
        <location filename="../ui/labelslider.cpp" line="271"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/labelslider.cpp" line="275"/>
        <source>&amp;Reset to Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/labelslider.cpp" line="306"/>
        <location filename="../ui/labelslider.cpp" line="345"/>
        <source>Set Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/labelslider.cpp" line="307"/>
        <location filename="../ui/labelslider.cpp" line="346"/>
        <source>New value:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoadDialog</name>
    <message>
        <location filename="../dialogs/loaddialog.cpp" line="37"/>
        <source>Loading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/loaddialog.cpp" line="42"/>
        <source>Loading &apos;%1&apos;...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/loaddialog.cpp" line="48"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoadThread</name>
    <message>
        <location filename="../project/loadthread.cpp" line="249"/>
        <source>Version Mismatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="250"/>
        <source>This project was saved in a different version of Olive and may not be fully compatible with this version. Would you like to attempt loading it anyway?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="573"/>
        <source>Invalid Clip Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="574"/>
        <source>This project contains an invalid clip link. It may be corrupt. Would you like to continue loading it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="697"/>
        <source>%1 - Line: %2 Col: %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="724"/>
        <source>User aborted loading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="755"/>
        <source>XML Parsing Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="756"/>
        <source>Couldn&apos;t load &apos;%1&apos;. %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="760"/>
        <source>Project Load Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/loadthread.cpp" line="761"/>
        <source>Error loading project: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui/mainwindow.cpp" line="303"/>
        <source>Welcome to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="862"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="863"/>
        <source>&amp;New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="864"/>
        <source>&amp;Open Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="865"/>
        <source>Clear Recent List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="866"/>
        <source>Open Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="867"/>
        <source>&amp;Save Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="868"/>
        <source>Save Project &amp;As</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="870"/>
        <source>&amp;Import...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="871"/>
        <source>&amp;Export...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="872"/>
        <source>E&amp;xit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="874"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="876"/>
        <source>&amp;Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="877"/>
        <source>Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="879"/>
        <source>Select &amp;All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="880"/>
        <source>Deselect All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="882"/>
        <source>Ripple to In Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="883"/>
        <source>Ripple to Out Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="884"/>
        <source>Edit to In Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="885"/>
        <source>Edit to Out Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="886"/>
        <source>Delete In/Out Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="887"/>
        <source>Ripple Delete In/Out Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="891"/>
        <source>&amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="892"/>
        <source>Zoom In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="893"/>
        <source>Zoom Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="894"/>
        <source>Increase Track Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="895"/>
        <source>Decrease Track Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="896"/>
        <source>Toggle Show All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="899"/>
        <source>Frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="900"/>
        <source>Drop Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="901"/>
        <source>Non-Drop Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="902"/>
        <source>Milliseconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="904"/>
        <source>Title/Action Safe Area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="905"/>
        <source>Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="906"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="907"/>
        <source>4:3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="908"/>
        <source>16:9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="909"/>
        <source>Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="911"/>
        <source>Full Screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="912"/>
        <source>Full Screen Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="914"/>
        <source>&amp;Playback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="915"/>
        <source>Go to Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="916"/>
        <source>Previous Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="917"/>
        <source>Play/Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="918"/>
        <source>Play In to Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="919"/>
        <source>Next Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="920"/>
        <source>Go to End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="922"/>
        <source>Go to Previous Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="923"/>
        <source>Go to Next Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="924"/>
        <source>Go to In Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="925"/>
        <source>Go to Out Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="927"/>
        <source>Shuttle Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="928"/>
        <source>Shuttle Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="929"/>
        <source>Shuttle Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="931"/>
        <source>Loop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="933"/>
        <source>&amp;Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="935"/>
        <source>Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="936"/>
        <source>Effect Controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="937"/>
        <source>Timeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="939"/>
        <source>Graph Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="941"/>
        <source>Media Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="942"/>
        <source>Sequence Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="944"/>
        <source>Maximize Panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="945"/>
        <source>Lock Panels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="946"/>
        <source>Reset to Default Layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="948"/>
        <source>&amp;Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="949"/>
        <source>Pointer Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="950"/>
        <source>Edit Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="951"/>
        <source>Ripple Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="952"/>
        <source>Razor Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="953"/>
        <source>Slip Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="954"/>
        <source>Slide Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="955"/>
        <source>Hand Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="956"/>
        <source>Transition Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="957"/>
        <source>Enable Snapping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="958"/>
        <source>Auto-Cut Silence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="960"/>
        <source>No Auto-Scroll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="961"/>
        <source>Page Auto-Scroll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="962"/>
        <source>Smooth Auto-Scroll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="869"/>
        <location filename="../ui/mainwindow.cpp" line="964"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="966"/>
        <source>Clear Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="970"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="972"/>
        <source>A&amp;ction Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="974"/>
        <source>Debug Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="976"/>
        <source>Effect Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="977"/>
        <source>Check Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="978"/>
        <source>Official Site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="979"/>
        <source>&amp;About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="980"/>
        <source>User Guide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="999"/>
        <source>&lt;untitled&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Marker</name>
    <message>
        <location filename="../timeline/marker.cpp" line="64"/>
        <source>Set Marker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../timeline/marker.cpp" line="66"/>
        <source>Set clip marker name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../timeline/marker.cpp" line="67"/>
        <source>Set sequence marker name:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Media</name>
    <message>
        <location filename="../project/media.cpp" line="95"/>
        <source>New Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="120"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="120"/>
        <source>Filename:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="124"/>
        <source>Video Dimensions:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="134"/>
        <source>Frame Rate:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="144"/>
        <source>%1 field(s) (%2 frame(s))</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="153"/>
        <source>Interlacing:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="165"/>
        <source>Audio Frequency:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="174"/>
        <source>Audio Channels:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="192"/>
        <source>Name: %1
Video Dimensions: %2x%3
Frame Rate: %4
Audio Frequency: %5
Audio Layout: %6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="322"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="324"/>
        <source>Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/media.cpp" line="328"/>
        <source>Rate</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MediaPropertiesDialog</name>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="44"/>
        <source>&quot;%1&quot; Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="53"/>
        <source>Tracks:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="61"/>
        <source>Video %1: %2x%3 %4FPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="77"/>
        <source>Audio %1: %2Hz %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="80"/>
        <source>%n channel(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="95"/>
        <source>Conform to Frame Rate:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="105"/>
        <source>Alpha is Premultiplied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="114"/>
        <source>Auto (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="127"/>
        <source>Interlacing:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/mediapropertiesdialog.cpp" line="134"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MenuHelper</name>
    <message>
        <location filename="../ui/menuhelper.cpp" line="169"/>
        <source>&amp;Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="170"/>
        <source>&amp;Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="171"/>
        <source>&amp;Sequence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="172"/>
        <source>&amp;Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="173"/>
        <source>Set In Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="174"/>
        <source>Set Out Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="175"/>
        <source>Reset In Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="176"/>
        <source>Reset Out Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="177"/>
        <source>Clear In/Out Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="178"/>
        <source>Add Default Transition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="179"/>
        <source>Link/Unlink</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="180"/>
        <source>Enable/Disable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="181"/>
        <source>Enable/Disable track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="182"/>
        <source>Nest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="183"/>
        <source>Cu&amp;t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="184"/>
        <source>Cop&amp;y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="185"/>
        <location filename="../ui/menuhelper.cpp" line="280"/>
        <source>&amp;Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="186"/>
        <source>Paste Insert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="187"/>
        <source>Duplicate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="188"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="189"/>
        <source>Ripple Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="190"/>
        <source>Split</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="233"/>
        <source>Invalid aspect ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="233"/>
        <source>The aspect ratio &apos;%1&apos; is invalid. Please try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="236"/>
        <source>Enter custom aspect ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menuhelper.cpp" line="236"/>
        <source>Enter the aspect ratio to use for the title/action safe area (e.g. 16:9):</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewProjDialog</name>
    <message>
        <location filename="../dialogs/newproj.cpp" line="60"/>
        <source>Recent projects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newproj.cpp" line="102"/>
        <source>open other project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newproj.cpp" line="49"/>
        <location filename="../dialogs/newproj.cpp" line="107"/>
        <source>create new project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newproj.cpp" line="47"/>
        <source>Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newproj.cpp" line="93"/>
        <source>none</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newproj.cpp" line="138"/>
        <source>project name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newproj.cpp" line="138"/>
        <source>Project name cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newproj.cpp" line="161"/>
        <source>Project Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newproj.cpp" line="168"/>
        <source>customize</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewSequenceDialog</name>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="62"/>
        <source>Editing &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="82"/>
        <source>New Sequence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="209"/>
        <source>Preset:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="213"/>
        <source>Film 4K</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="214"/>
        <source>1080p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="215"/>
        <source>V720p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="216"/>
        <source>720p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="217"/>
        <source>480p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="218"/>
        <source>360p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="219"/>
        <source>240p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="220"/>
        <source>144p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="221"/>
        <source>NTSC (480i)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="222"/>
        <source>PAL (576i)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="223"/>
        <source>Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="231"/>
        <source>Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="235"/>
        <source>Width:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="241"/>
        <source>Height:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="246"/>
        <source>Frame Rate:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="266"/>
        <source>Pixel Aspect Ratio:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="268"/>
        <source>Square Pixels (1.0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="271"/>
        <source>Interlacing:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="273"/>
        <source>None (Progressive)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="281"/>
        <source>Audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="285"/>
        <source>Sample Rate: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/newsequencedialog.cpp" line="303"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OliveGlobal</name>
    <message>
        <location filename="../global/global.cpp" line="72"/>
        <location filename="../global/global.cpp" line="100"/>
        <source>KunGang Project %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="69"/>
        <source>KunGang %1%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="99"/>
        <source>KunGang %1.%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="109"/>
        <source>Auto-recovery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="110"/>
        <source>KunGang didn&apos;t close properly and an autorecovery file was detected. Would you like to open it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="265"/>
        <source>Open Project...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="278"/>
        <source>Missing recent project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="279"/>
        <source>The project &apos;%1&apos; no longer exists. Would you like to remove it from the recent projects list?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="290"/>
        <source>Save Project As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="315"/>
        <source>replace project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="316"/>
        <source>file is existing, do you want to overwrite it? </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="347"/>
        <source>Unsaved Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="348"/>
        <source>This project has changed since it was last saved. Would you like to save it before closing?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="379"/>
        <source>Missing Project File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="380"/>
        <source>Specified project &apos;%1&apos; does not exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="444"/>
        <source>No active sequence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="445"/>
        <source>Please open the sequence to perform this action.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="510"/>
        <source>No clips selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/global.cpp" line="511"/>
        <source>Select the clips you wish to auto-cut</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PanEffect</name>
    <message>
        <location filename="../effects/internal/paneffect.cpp" line="32"/>
        <source>Pan</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="84"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="91"/>
        <source>Default Sequence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="196"/>
        <source>Invalid CSS File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="197"/>
        <source>CSS file &apos;%1&apos; does not exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="227"/>
        <source>Restart Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="228"/>
        <source>Would you like to restart Kungang to make some settings take effect now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="365"/>
        <source>Confirm Reset All Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="366"/>
        <source>Are you sure you wish to reset all keyboard shortcuts to their defaults?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="416"/>
        <source>Import Keyboard Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="440"/>
        <location filename="../dialogs/preferencesdialog.cpp" line="464"/>
        <source>Error saving shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="441"/>
        <source>Failed to open file for reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="448"/>
        <source>Export Keyboard Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="462"/>
        <source>Export Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="462"/>
        <source>Shortcuts exported successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="464"/>
        <source>Failed to open file for writing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="470"/>
        <source>Browse for CSS file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="478"/>
        <source>Delete All Previews</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="479"/>
        <source>Are you sure you want to delete all previews?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="483"/>
        <source>Previews Deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="484"/>
        <source>All previews deleted successfully. You may have to re-open your current project for changes to take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="508"/>
        <source>Language:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="544"/>
        <source>Image sequence formats:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="554"/>
        <source>Thumbnail Resolution:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="562"/>
        <source>Waveform Resolution:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="570"/>
        <source>Delete Previews</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="575"/>
        <source>Default frames for image:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="585"/>
        <source>Use Software Fallbacks When Possible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="592"/>
        <source>Default Sequence Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="596"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="600"/>
        <source>Behavior</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="604"/>
        <source>Add Default Effects to New Clips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="608"/>
        <source>Automatically Seek to the Beginning When Playing at the End of a Sequence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="612"/>
        <source>Selecting Also Seeks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="616"/>
        <source>Edit Tool Also Seeks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="620"/>
        <source>Edit Tool Selects Links</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="624"/>
        <source>Seek Also Selects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="628"/>
        <source>Seek to the End of Pastes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="632"/>
        <source>Scroll Wheel Zooms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="633"/>
        <source>Hold CTRL to toggle this setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="637"/>
        <source>Invert Timeline Scroll Axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="641"/>
        <source>Enable Drag Files to Timeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="645"/>
        <source>Auto-Scale By Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="649"/>
        <source>Auto-Seek to Imported Clips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="653"/>
        <source>Audio Scrubbing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="657"/>
        <source>Drop Files on Media to Replace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="661"/>
        <source>Enable Hover Focus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="665"/>
        <source>Ask For Name When Setting Marker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="671"/>
        <source>Appearance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="678"/>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="681"/>
        <source>Kungang Dark (Default)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="682"/>
        <source>Kungang Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="693"/>
        <source>Use Native Menu Styling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="701"/>
        <source>Custom CSS:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="707"/>
        <source>Browse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="714"/>
        <source>Effect Textbox Lines:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="729"/>
        <source>Memory Usage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="731"/>
        <source>Upcoming Frame Queue:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="736"/>
        <location filename="../dialogs/preferencesdialog.cpp" line="745"/>
        <source>frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="737"/>
        <location filename="../dialogs/preferencesdialog.cpp" line="746"/>
        <source>seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="740"/>
        <source>Previous Frame Queue:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="751"/>
        <source>Playback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="762"/>
        <source>Output Device:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="765"/>
        <location filename="../dialogs/preferencesdialog.cpp" line="788"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="785"/>
        <source>Input Device:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="808"/>
        <source>Sample Rate:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="824"/>
        <source>Audio Recording:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="827"/>
        <source>Mono</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="828"/>
        <source>Stereo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="834"/>
        <source>Audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="842"/>
        <source>Search for action or shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="849"/>
        <source>Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="850"/>
        <source>Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="855"/>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="859"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="865"/>
        <source>Reset Selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="869"/>
        <source>Reset All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferencesdialog.cpp" line="875"/>
        <source>Keyboard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PreviewGenerator</name>
    <message>
        <location filename="../project/previewgenerator.cpp" line="205"/>
        <source>Failed to find any valid video/audio streams</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/previewgenerator.cpp" line="561"/>
        <source>Could not open file - %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/previewgenerator.cpp" line="568"/>
        <source>Could not find stream information - %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Project</name>
    <message>
        <location filename="../panels/project.cpp" line="99"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="105"/>
        <source>Open Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="111"/>
        <source>Save Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="117"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="123"/>
        <source>Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="134"/>
        <source>Tree View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="140"/>
        <source>Icon View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="146"/>
        <source>List View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="224"/>
        <source>Search media, markers, etc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="225"/>
        <source>Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="229"/>
        <source>Sequence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="351"/>
        <source>Replace &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="353"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="364"/>
        <location filename="../panels/project.cpp" line="923"/>
        <source>No active sequence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="365"/>
        <source>No sequence is active, please open the sequence you want to replace clips from.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="373"/>
        <source>Active sequence selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="374"/>
        <source>You cannot insert a sequence into itself, so no clips of this media would be in this sequence.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="405"/>
        <source>Rename &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="406"/>
        <source>Enter new name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="577"/>
        <source>Delete media in use?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="578"/>
        <source>The media &apos;%1&apos; is currently used in &apos;%2&apos;. Deleting it will remove all instances in the sequence. Are you sure you want to do this?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="581"/>
        <source>Skip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="750"/>
        <source>Import a Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="751"/>
        <source>&quot;%1&quot; is an KunGang project file. It will merge with this project. Do you wish to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="911"/>
        <source>Import media...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="911"/>
        <source>Media Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/project.cpp" line="924"/>
        <source>No sequence is active, please open the sequence you want to delete clips from.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProxyDialog</name>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="41"/>
        <source>Create Proxy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="44"/>
        <source>Proxy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="50"/>
        <source>Dimensions:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="53"/>
        <source>Same Size as Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="54"/>
        <source>Half Resolution (1/2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="55"/>
        <source>Quarter Resolution (1/4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="56"/>
        <source>Eighth Resolution (1/8)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="57"/>
        <source>Sixteenth Resolution (1/16)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="61"/>
        <source>Format:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="64"/>
        <source>ProRes HQ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="72"/>
        <source>Location:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="75"/>
        <source>Same as Source (in &quot;%1&quot; folder)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="128"/>
        <source>Proxy file exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="129"/>
        <source>The file &quot;%1&quot; already exists. Do you wish to replace it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/proxydialog.cpp" line="182"/>
        <source>Custom Location</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProxyGenerator</name>
    <message>
        <location filename="../project/proxygenerator.cpp" line="332"/>
        <source>Finished generating proxy for &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../effects/effectloaders.cpp" line="56"/>
        <source>Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="61"/>
        <source>Pan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="70"/>
        <source>Tone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="75"/>
        <source>Noise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="80"/>
        <source>Fill Left/Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="88"/>
        <source>Transform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="89"/>
        <source>Distort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="94"/>
        <source>Corner Pin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="103"/>
        <source>Shake</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="109"/>
        <source>Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="110"/>
        <location filename="../effects/effectloaders.cpp" line="117"/>
        <source>Render</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="116"/>
        <source>Rich Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="122"/>
        <source>Timecode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="127"/>
        <source>Solid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="136"/>
        <source>Cross Dissolve</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="143"/>
        <source>Linear Fade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="148"/>
        <source>Exponential Fade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/effectloaders.cpp" line="153"/>
        <source>Logarithmic Fade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="436"/>
        <location filename="../global/config.cpp" line="481"/>
        <source>Radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="437"/>
        <location filename="../global/config.cpp" line="479"/>
        <source>Horizontal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="438"/>
        <location filename="../global/config.cpp" line="480"/>
        <location filename="../global/config.cpp" line="525"/>
        <source>Vertical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="439"/>
        <source>Amount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="440"/>
        <location filename="../global/config.cpp" line="497"/>
        <location filename="../global/config.cpp" line="504"/>
        <location filename="../global/config.cpp" line="510"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="441"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="442"/>
        <source>Key Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="443"/>
        <source>Lower Tolerance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="444"/>
        <source>Upper Tolerance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="445"/>
        <source>Red Amount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="446"/>
        <source>Green Amount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="447"/>
        <source>Blue Amount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="448"/>
        <source>Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="449"/>
        <source>Tint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="450"/>
        <source>Exposure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="451"/>
        <location filename="../global/config.cpp" line="474"/>
        <source>Contrast</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="452"/>
        <source>Highlights</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="453"/>
        <source>Shadows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="454"/>
        <source>Whites (Brightness)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="455"/>
        <source>Blacks (Darkness)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="456"/>
        <location filename="../global/config.cpp" line="484"/>
        <location filename="../global/config.cpp" line="532"/>
        <source>Saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="457"/>
        <source>Find by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="458"/>
        <location filename="../global/config.cpp" line="487"/>
        <source>Lower Limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="459"/>
        <location filename="../global/config.cpp" line="488"/>
        <source>Upper Limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="460"/>
        <location filename="../global/config.cpp" line="466"/>
        <location filename="../global/config.cpp" line="468"/>
        <location filename="../global/config.cpp" line="477"/>
        <location filename="../global/config.cpp" line="489"/>
        <location filename="../global/config.cpp" line="521"/>
        <source>Invert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="461"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="462"/>
        <source>Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="463"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="464"/>
        <source>Bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="465"/>
        <source>Feather</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="467"/>
        <location filename="../global/config.cpp" line="478"/>
        <location filename="../global/config.cpp" line="518"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="469"/>
        <source>Channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="470"/>
        <source>Factor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="471"/>
        <source>Balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="472"/>
        <source>Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="473"/>
        <location filename="../global/config.cpp" line="508"/>
        <source>Angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="475"/>
        <source>Resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="476"/>
        <source>HSL mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="482"/>
        <source>Sigma</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="483"/>
        <location filename="../global/config.cpp" line="531"/>
        <source>Hue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="485"/>
        <source>Brightness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="490"/>
        <source>Color Noise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="491"/>
        <source>Blend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="492"/>
        <source>Horizontal Pixels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="493"/>
        <source>Vertical Pixels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="494"/>
        <source>Bypass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="495"/>
        <location filename="../global/config.cpp" line="513"/>
        <source>Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="496"/>
        <source>Gamma</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="498"/>
        <source>Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="499"/>
        <location filename="../global/config.cpp" line="523"/>
        <source>Intensity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="500"/>
        <location filename="../global/config.cpp" line="522"/>
        <source>Frequency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="501"/>
        <source>Reverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="502"/>
        <location filename="../global/config.cpp" line="507"/>
        <source>Stretch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="505"/>
        <source>Tile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="506"/>
        <source>Hide Edges</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="509"/>
        <source>Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="511"/>
        <source>Mirror X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="512"/>
        <source>Mirror Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="514"/>
        <source>Saturation Levels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="515"/>
        <source>Brightness Levels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="516"/>
        <source>Edge Tolerance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="517"/>
        <source>Edge Strength</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="519"/>
        <source>Softness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="520"/>
        <source>Circular</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="524"/>
        <source>Evolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="526"/>
        <source>Composite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="527"/>
        <source>Alpha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="528"/>
        <source>Original</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="529"/>
        <source>Luminance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="530"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="533"/>
        <source>Red</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="534"/>
        <source>Green</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../global/config.cpp" line="535"/>
        <source>Blue</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ReplaceClipMediaDialog</name>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="37"/>
        <source>Replace clips using &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="43"/>
        <source>Select which media you want to replace this media&apos;s clips with:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="49"/>
        <source>Keep the same media in-points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="57"/>
        <source>Replace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="61"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="77"/>
        <source>No media selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="78"/>
        <source>Please select a media to replace with or click &apos;Cancel&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="86"/>
        <source>Same media selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="87"/>
        <source>You selected the same media that you&apos;re replacing. Please select a different one or click &apos;Cancel&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="93"/>
        <source>Folder selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="94"/>
        <source>You cannot replace footage with a folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="101"/>
        <source>Active sequence selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/replaceclipmediadialog.cpp" line="102"/>
        <source>You cannot insert a sequence into itself.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RichTextEffect</name>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="22"/>
        <source>Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="26"/>
        <source>Padding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="30"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="34"/>
        <source>Vertical Align:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="36"/>
        <source>Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="37"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="38"/>
        <source>Bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="42"/>
        <source>Auto-Scroll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="44"/>
        <source>Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="45"/>
        <source>Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="46"/>
        <source>Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="47"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="48"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="51"/>
        <source>Shadow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="55"/>
        <source>Shadow Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="59"/>
        <source>Shadow Angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="63"/>
        <source>Shadow Distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="68"/>
        <source>Shadow Softness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/richtexteffect.cpp" line="73"/>
        <source>Shadow Opacity</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Sequence</name>
    <message>
        <location filename="../timeline/sequence.cpp" line="41"/>
        <source>%1 (copy)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShakeEffect</name>
    <message>
        <location filename="../effects/internal/shakeeffect.cpp" line="38"/>
        <source>Intensity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/shakeeffect.cpp" line="43"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/shakeeffect.cpp" line="48"/>
        <source>Frequency</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SolidEffect</name>
    <message>
        <location filename="../effects/internal/solideffect.cpp" line="43"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/solideffect.cpp" line="45"/>
        <source>Solid Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/solideffect.cpp" line="46"/>
        <source>SMPTE Bars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/solideffect.cpp" line="47"/>
        <source>Checkerboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/solideffect.cpp" line="49"/>
        <source>Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/solideffect.cpp" line="55"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/solideffect.cpp" line="59"/>
        <source>Checkerboard Size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SourcesCommon</name>
    <message>
        <location filename="../project/sourcescommon.cpp" line="83"/>
        <source>Import...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="86"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="90"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="93"/>
        <source>Tree View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="96"/>
        <source>Icon View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="99"/>
        <source>Show Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="104"/>
        <source>Show Sequences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="116"/>
        <source>Replace/Relink Media</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="120"/>
        <source>Reveal in Explorer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="122"/>
        <source>Reveal in Finder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="124"/>
        <source>Reveal in File Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="129"/>
        <source>Replace Clips Using This Media</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="152"/>
        <source>Create Sequence With This Media</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="158"/>
        <source>Duplicate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="164"/>
        <source>Delete All Clips Using This Media</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="167"/>
        <source>Proxy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="174"/>
        <source>Generating proxy: %1% complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="198"/>
        <source>Create/Modify Proxy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="201"/>
        <source>Create Proxy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="212"/>
        <source>Modify Proxy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="215"/>
        <source>Restore Original</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="221"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="228"/>
        <source>Preview in Media Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="234"/>
        <source>Properties...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="291"/>
        <source>Replace Media</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="292"/>
        <source>You dropped a file onto &apos;%1&apos;. Would you like to replace it with the dropped file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="422"/>
        <source>Delete proxy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../project/sourcescommon.cpp" line="423"/>
        <source>Would you like to delete the proxy file &quot;%1&quot; as well?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SpeedDialog</name>
    <message>
        <location filename="../dialogs/speeddialog.cpp" line="40"/>
        <source>Speed/Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/speeddialog.cpp" line="49"/>
        <source>Speed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/speeddialog.cpp" line="56"/>
        <source>Frame Rate:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/speeddialog.cpp" line="61"/>
        <source>Duration:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/speeddialog.cpp" line="69"/>
        <source>Reverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/speeddialog.cpp" line="70"/>
        <source>Maintain Audio Pitch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/speeddialog.cpp" line="71"/>
        <source>Ripple Changes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextEditDialog</name>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="35"/>
        <source>Edit Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="68"/>
        <source>Thin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="69"/>
        <source>Extra Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="70"/>
        <source>Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="71"/>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="72"/>
        <source>Medium</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="73"/>
        <source>Demi Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="74"/>
        <source>Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="75"/>
        <source>Extra Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/texteditdialog.cpp" line="76"/>
        <source>Black</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextEditEx</name>
    <message>
        <location filename="../ui/texteditex.cpp" line="43"/>
        <source>Edit Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/texteditex.cpp" line="91"/>
        <source>&amp;Edit Text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextEffect</name>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="51"/>
        <source>Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="55"/>
        <source>Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="59"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="64"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="68"/>
        <source>Alignment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="70"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="71"/>
        <location filename="../effects/internal/texteffect.cpp" line="77"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="72"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="73"/>
        <source>Justify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="76"/>
        <source>Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="78"/>
        <source>Bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="80"/>
        <source>Word Wrap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="84"/>
        <source>Padding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="88"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="92"/>
        <source>Outline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="96"/>
        <source>Outline Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="100"/>
        <source>Outline Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="105"/>
        <source>Shadow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="109"/>
        <source>Shadow Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="113"/>
        <source>Shadow Angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="117"/>
        <source>Shadow Distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="122"/>
        <source>Shadow Softness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="127"/>
        <source>Shadow Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/texteffect.cpp" line="134"/>
        <source>Sample Text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TimecodeEffect</name>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="51"/>
        <source>Timecode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="53"/>
        <source>Sequence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="54"/>
        <source>Media</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="57"/>
        <source>Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="64"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="69"/>
        <source>Background Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="74"/>
        <source>Background Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="81"/>
        <source>Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/timecodeeffect.cpp" line="85"/>
        <source>Prepend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Timeline</name>
    <message>
        <location filename="../panels/timeline.cpp" line="126"/>
        <source>Pointer Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="131"/>
        <source>Edit Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="132"/>
        <source>Ripple Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="127"/>
        <location filename="../panels/timeline.cpp" line="133"/>
        <source>Razor Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="128"/>
        <source>CC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="129"/>
        <source>Solid Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="134"/>
        <source>Slip Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="135"/>
        <source>Slide Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="136"/>
        <source>Hand Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="138"/>
        <source>Transition Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="139"/>
        <source>Snapping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="140"/>
        <source>Zoom In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="141"/>
        <source>Zoom Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="142"/>
        <source>Record audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="143"/>
        <source>Add title, solid, bars, etc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="497"/>
        <source>Nested Sequence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1393"/>
        <source>Effect already exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1394"/>
        <source>Clip &apos;%1&apos; already contains a &apos;%2&apos; effect. Would you like to replace it with the pasted one or add it as a separate effect?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1399"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1400"/>
        <source>Replace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1401"/>
        <source>Skip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1403"/>
        <source>Do this for all conflicts found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1866"/>
        <source>Title...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1871"/>
        <source>Solid Color...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1876"/>
        <source>Bars...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1883"/>
        <source>Tone...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1888"/>
        <source>Noise...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1922"/>
        <source>Unsaved Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1923"/>
        <source>You must save this project before you can record audio in it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1929"/>
        <source>Click on the timeline where you want to start recording (drag to limit the recording to a certain timeframe)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1990"/>
        <source>Timeline: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/timeline.cpp" line="1992"/>
        <source>(none)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TimelineHeader</name>
    <message>
        <location filename="../ui/timelineheader.cpp" line="486"/>
        <source>Center Timecodes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TimelineWidget</name>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="90"/>
        <source>&amp;Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="91"/>
        <source>&amp;Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="111"/>
        <source>R&amp;ipple Delete Empty Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="115"/>
        <source>Sequence Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="134"/>
        <source>&amp;Speed/Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="137"/>
        <source>Auto-Cut Silence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="140"/>
        <source>Auto-S&amp;cale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="175"/>
        <source>&amp;Reveal in Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="179"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="207"/>
        <source>%1
Start: %2
End: %3
Duration: %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="233"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="233"/>
        <source>Couldn&apos;t locate media wrapper for sequence.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="531"/>
        <source>New Sequence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="532"/>
        <source>No sequence has been created yet. Would you like to make one based on this footage or set custom parameters?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="534"/>
        <source>Use Footage Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="535"/>
        <source>Custom Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="1076"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="1080"/>
        <source>Solid Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="1085"/>
        <source>Bars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="1096"/>
        <source>Tone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="1100"/>
        <source>Noise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/timelinewidget.cpp" line="2029"/>
        <source>Duration:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToneEffect</name>
    <message>
        <location filename="../effects/internal/toneeffect.cpp" line="31"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/toneeffect.cpp" line="33"/>
        <source>Sine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/toneeffect.cpp" line="35"/>
        <source>Frequency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/toneeffect.cpp" line="41"/>
        <source>Amount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/toneeffect.cpp" line="47"/>
        <source>Mix</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TransformEffect</name>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="49"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="54"/>
        <source>Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="64"/>
        <source>Uniform Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="68"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="72"/>
        <source>Anchor Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="77"/>
        <source>Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="84"/>
        <source>Blend Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/transformeffect.cpp" line="89"/>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Transition</name>
    <message>
        <location filename="../effects/transition.cpp" line="48"/>
        <source>Length</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UpdateNotification</name>
    <message>
        <location filename="../ui/updatenotification.cpp" line="160"/>
        <source>There is new version:%1.%2 for download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/updatenotification.cpp" line="165"/>
        <source>Latest version</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VSTHost</name>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="130"/>
        <location filename="../effects/internal/vsthost.cpp" line="146"/>
        <source>Error loading VST plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="131"/>
        <source>Failed to load VST plugin &quot;%1&quot;: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="147"/>
        <source>Failed to locate entry point for dynamic library.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="172"/>
        <source>VST Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="172"/>
        <source>Plugin&apos;s magic number is invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="228"/>
        <source>VST Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="254"/>
        <source>Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="258"/>
        <source>Interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/vsthost.cpp" line="260"/>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Viewer</name>
    <message>
        <location filename="../panels/viewer.cpp" line="603"/>
        <source>(none)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/viewer.cpp" line="739"/>
        <source>Drag video only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panels/viewer.cpp" line="746"/>
        <source>Drag audio only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="981"/>
        <source>Sequence Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="982"/>
        <source>Media Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="984"/>
        <source>Notice Viewer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ViewerWidget</name>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="113"/>
        <source>Save Frame as Image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="116"/>
        <source>Show Fullscreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="120"/>
        <source>Disable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="123"/>
        <source>Screen %1: %2x%3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="131"/>
        <source>Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="132"/>
        <source>Fit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="142"/>
        <source>Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="148"/>
        <source>Close Media</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="158"/>
        <source>Save Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="192"/>
        <source>Viewer Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/viewerwidget.cpp" line="193"/>
        <source>Set Custom Zoom Value:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ViewerWindow</name>
    <message>
        <location filename="../ui/viewerwindow.cpp" line="170"/>
        <source>Exit Fullscreen</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VoidEffect</name>
    <message>
        <location filename="../effects/internal/voideffect.cpp" line="33"/>
        <source>(unknown)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/internal/voideffect.cpp" line="37"/>
        <source>Missing Effect</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VolumeEffect</name>
    <message>
        <location filename="../effects/internal/volumeeffect.cpp" line="32"/>
        <source>Volume</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>transition</name>
    <message>
        <location filename="../effects/transition.cpp" line="117"/>
        <source>Invalid transition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../effects/transition.cpp" line="118"/>
        <source>No candidate for transition &apos;%1&apos;. This transition may be corrupt. Try reinstalling it or Olive.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
